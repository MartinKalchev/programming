#include <iostream>
#include <string>
#include <limits>

using namespace std;


int main()
{
    int x;
    int minimum = numeric_limits<int>::max();
    int maximum = numeric_limits<int>::min();
    while(cin>>x)
    {
        if(minimum > x) minimum = x;
        if(maximum < x) maximum = x;
    }

cout << "min value: " << minimum << endl
<< "max value: " << maximum << endl;

}
