#include <iostream>
#include <cmath>

using namespace std;

void fillMask(int* mask,int m_size,int val);
void deleteMasks(int**& masks,int& m_size);
void createMask(char* u_set,int u_size,int**& masks,int& m_size);

void printMasks(int* m,int m_size)
{
    for(int i = 0; i < m_size;i++)
    {
        cout << m[i] << " ";
    }
}


int main()
{
    char* u_set = NULL;
    int u_size = 0;

    cout << "Enter the size of chars in set: ";
    cin >> u_size;

    u_set = new char[u_size];
    cout << "Enter char values: ";
    for(int i=0;i < u_size;i++)
    {
        cin >> u_set[i];
    }

    int** masks = NULL;

    int m_size = 0;

    createMask(u_set,u_size,masks,m_size);

    cout << "Mask count " << m_size << endl;

    for(int i = 0;i < m_size;i++)
    {
        printMasks(masks[i],u_size);
        cout << endl;
    }

    deleteMasks(masks,m_size);

    return 0;
}


void fillMask(int* mask,int m_size,int val)
{
    for(int i = 0;i < m_size;i++)
    {
        mask[m_size - 1 - i] = val%2;
        val = val/2;
    }
}

void createMask(char* u_set,int u_size,int**& masks,int& m_size)
{
    int cnt = pow(2,u_size);

    cout << "Count: " << cnt << endl;

    masks = new int*[cnt];

    for(int i =0;i < cnt;i++)
    {
        masks[i] = new int[u_size];

        fillMask(masks[i],u_size,i);
    }
    m_size = cnt;
}

void deleteMasks(int**& masks,int& m_size)
{
    for(int i = 0;i < m_size;i++)
    {
        delete[] masks[i];
        masks[i] = NULL;
    }
    delete[] masks;
    masks = NULL;
        m_size = 0;
}









