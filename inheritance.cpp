#include <iostream>
#include <string>
using namespace std;

class Car
{
public:
    Car(string br);

    string get_brand() const;

private:
    string brand;
};

Car::Car(string br)
{
    brand = br;
}

string Car::get_brand() const
{
   return brand;
}

class Student
{
public:

    Student(int fnum,string nm,Car* pC)
    {
        fn = fnum;
        name = nm;
        pCar = pC;
    }

    string get_info()const
    {
        return name + " " + pCar->get_brand();
    }

    void set_car(Car* pC)
    {
        pCar = pC;
    }

private:
    unsigned int fn;
    string name;
    Car* pCar;
};


int main()
{
Car nissan("Nissan GTR");
cout << nissan.get_brand() << endl;
Car audi("Audi");

Student martin(85994,"Martin",&nissan);
cout << martin.get_info() << endl;
martin.set_car(&audi);
cout << martin.get_info()<< endl;
return 0;
}
