/* 
Output: Enter a tank size:

Input: 50

Output: Enter fuel efficiency:

Input: 10

Output: Enter price per liter:

Input: 2.45 */


#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
 cout << "Enter the tank size of your car: ";
 float tank = 0;
 cin >> tank;
 
 cout << "Enter the fuel efficiency of your car (km per liter): ";
 float eff = 0;
 cin >> eff;
 
 cout << "Enter the price per liter: ";
 float price = 0;
 cin >> price;
 

 cout << setprecision(4)  << "We can travel " << tank*100/eff << " km" << endl;
 cout << "For every 100km it will cost " << price*eff << " lv" << endl;
 
 return 0;
}