#include <iostream>

using namespace std;

/**
The user enters 3 numbers and then it is checked if these numbers can be the sides of a triangle
*/


int main()
{
   int a,b,c;
   cin >> a >> b >> c;

   if((a < b + c) && (b < a + c) && (c < a + b) )
   {
       cout << "Triangle" << endl;
   }else
   cout << "Cannot be a triangle";

   return 0;
}
