#include <iostream>
#include <string>
#include <limits>

using namespace std;


int main()
{
    int years;
    float deposit,interest;

    cout << "Please enter the value of the deposit: ";
    cin >> deposit;

    cout << "Please enter the interest rate: ";
    cin >> interest;

    cout << "Please enter the years: ";
    cin >> years;


  for(int year = 0;year < years; year++ )
  {
      deposit += (1 + interest/100);
  }

  cout << "The deposit will be: " << deposit <<endl;
}
