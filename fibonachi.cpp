#include <iostream>

using namespace std;


int f_fib(int n)
{
    int f1 = 1;
    int f2 = 1;

    int resultf = 0;

    if(n < 3)
    {
        resultf = 1;
    }
    else{
            int i = 2;
        while(i < n)
        {
            resultf = f1 + f2;
            f1 = f2;
            f2 = resultf;
            i++;
        }
    }
    cout << "Fib(" << n << ") = " << resultf << endl;
    return resultf;
}



int main()
{
    int n, fibn;
    int m, fibm;

    cout << "Enter a number for Fib. sequence: ";
    cin >> n;

    fibn = f_fib(n);

    cout << "Enter a number for Fib. sequence: ";
    cin >> m;

    fibm = f_fib(m);


    return 0;
}
