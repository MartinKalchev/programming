#include <iostream>
#include <string>
#include <limits>

using namespace std;

int main()
{
  float f,min = numeric_limits<float>::max(), max = numeric_limits<float>::min();
  

  while(cin >> f)
  {
      if(f < min) min = f;
      if(f > max) max = f;
  }
  
cout << "minimum: " << min << endl;
cout << "maximum: " << max << endl;

return 0;
}
