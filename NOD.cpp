#include <iostream>
#include <string>
#include <cmath>


using namespace std;


int main()
{
   int a,b;
   cout << "Please enter the value of a: ";
   cin >> a;

   cout << "Please enter the value of b: ";
   cin >> b;

   if(a != 0 && b != 0)
   {
       a = (a > 0 ? a : -1*a);
       b = (b > 0 ? b : -1*b);

       int t;
       if(a < b)
       {
           t = a;
           a = b;
           b = t;
       }
       t = a % b;
       while(t != 0)
       {
           a = b;
           b = t;
           t = a % b;
       }

       cout << "NOD: " << b;

   }
   else{
       if(a == 0 && b == 0)
       {
           cout << "Cant calculate NOD" << endl;
       }
       else{
        cout << "NOD: " << (a != 0 ? a : b) << endl;
       }
   }

    return 0;
}
