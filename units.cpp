/* 
Output: Enter a number in meters:

Input: 600

Output: 

600 m = 0.6 km

600 m = 6000 dm

600 m == 60000 cm */


#include <iostream>

using namespace std;

int main()
{
 cout << "Enter a number in meters: ";
 int m = 0;
 cin >> m;
 
 cout << m << " m= " << m / 1000. << " km" << endl;
 cout << m << " dm= " << m * 10. << " dm" << endl;
 cout << m << " cm= " << m * 100. << " cm"; 
return 0;
}