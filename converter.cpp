#include <iostream>
#include <string>

// Converts from decimal to  binary system

using namespace std;

int main()
{
    int num;
    string r;

    cout << "Please input a decimal value: ";
    cin >> num;

    int d;
    while(num != 0)
    {
       d = num % 2;
       if(d == 0)
       {
           r = "0" + r;
       }
       else
       {
           r = "1" + r;
       }
      num = num / 2;
    }
    cout << r;
return 0;
}

