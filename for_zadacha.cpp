#include <iostream>
#include <cmath>

using namespace std;


int main()
{
  int n, prod = 1;
  cout << "Enter a number: " << endl;
  cin >> n;

  if(n > 0)
  {
      for(int i = 1; i <= n; i++)
      {
          prod *= i;
      }
  }else
  {
     cout << "Incorrect input";
  }

  cout << prod;
  return 0;
}
